import java.awt.event.*;
import javax.swing.Timer;

/**
 * Classe qui va s'occuper de gérer et observer les actions de l'utilisateur sur le jeu.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 */
public class ListenerKey implements KeyListener {

    //La classe qui s'occupe de modifier et gérer la fenêtre du jeu
    private Presentation pre;

    //La classe qui sert de chronomètre dans le jeu afin de gérer l'inactivité du joueur
    public Chrono chrono;


    /**
     * Constructeur de la classe, nécessite la classe de gestion de la fenêtre du jeu.
     * @param pre La classe de gestion de la fenêtre du jeu
     **/
    public ListenerKey ( Presentation pre ) {
        this.pre = pre;
        //ajout de la surveillance de fenêtre du jeu
        this.pre.getFenetre().addKeyListener(this);

        //création du timer
        Timer timer = new Timer(1000,null);
        //création du chrono qui surveille le timer, et réagit en communiquant avec cette classe
        this.chrono = new Chrono(timer,this);
        //ajout de l'action de surveiller le timer
        timer.addActionListener(this.chrono);
        //démarer le timer
        timer.start();
    }


    /**
     * Méthode exécuté lors de l'action d'une touche du clavier. La touche actionnée va être identifiée afin de savoir si l'action de cette touche entraîne une réaction du jeu.
     * L'objet événement créer lors d'une action de l'utilisateur
     */
    public void keyPressed(KeyEvent e) {
        //récupération du code de la touche pressée
        int code = e.getKeyCode();

        //boolean pour savoir si la touche presser concernant une action ne doit pas se passer en même temps que la descente automatique de la figure en cours
        boolean blockDownButtonPressed = false;


        //si la touche pressé est "flèche de gauche" ou "la lettre q"
        if ( code == KeyEvent.VK_LEFT || code == KeyEvent.VK_Q ) {
            //déplacement de la figure en cours vers la gauche
            this.moveLeft();
            //System.out.println("LEFT");

        //si la touche pressé est "la flèche du haut" ou "la lettre z"
        }else if ( code == KeyEvent.VK_UP || code == KeyEvent.VK_Z ) {
            //faire pivoter la figure en cours d'une rotation (de 90°)
            this.rotate();
            //System.out.println("UP");

        //si la touche pressé est "la flèche du droite" ou "la lettre d"
        }else if ( code == KeyEvent.VK_RIGHT || code == KeyEvent.VK_D ) {
            //déplacement de la figure en cours vers la droite
            this.moveRight();
            //System.out.println("Right");

        //si la touche pressé est "la flèche du bas" ou "la lettre s"
        }else if ( code == KeyEvent.VK_DOWN || code == KeyEvent.VK_S ) {
            //déplacement de la figure en cours vers le bas
            this.moveDown();
            //System.out.println("DOWN");
            //arrêt du timer pour éviter que la figure décente toute seule quelque microseconde après l'action de cette touche, sinon le joueur pensera déplacer la figure une fois vers le bas alors
            //qu'elle se déplacera deux fois ce qui peut surprendre le joueur
            this.chrono.stop();
            //affirmer que le timer est arrêté
            blockDownButtonPressed = true;

        //si la touche pressé est "shift / majuscule" ou "tab"
        }else if ( code == KeyEvent.VK_SHIFT || code == KeyEvent.VK_TAB ) {
            //permutation de la figure en cours et de la figure sauvé
            this.switchSave();
            //System.out.println("SHIFT");
        }

        //si le timer à été arrêter
        if ( blockDownButtonPressed ) {
            //ré-initialiser le timer, afin de compter l'action du joueur comme le départ d'un nouveau cycle pour le timer
            this.chrono.reInit();
        }else{
            this.chrono.start();
        }
    }

    //Méthodes obligatoirement implémenté à cause de l'implémentation de la classe KeyListener mais non utilisé pour le fonctionnement du jeu
    public void keyReleased(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}


    /**
     * Méthode pour échanger la piece en cours et la piece sauvegardé. Si aucune piece n'est sauvé, alors la piece en cours est
     * sauvé et la nouvelle piece en cours est la prochains figure à jouer.
     **/
    public void switchSave () {

        //si aucune figure n'est sauvé
        if ( this.pre.partie.getSave() == null ) {

            //sauvegarde de la pièce en cours
            this.pre.partie.setSave(this.pre.partie.enCours());

            //effacement de la figure en cours du tableau de jeu
            int[][] blocks = this.pre.partie.enCours().getOccupedCube();
            for ( int i = 0 ; i < blocks.length ; i++ ) {
                this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]] = null;
                this.pre.setColor(blocks[i][0],blocks[i][1]);
            }
            this.pre.partie.setEnCours(null);

            //afficher la figure sauvegardé dans le tableau de bouton affichant la figure sauvegardé
            this.pre.updateSaveBlock();
            this.pre.reframe();

            //faire passer la prochaine pièce à jouer comme pièce en cours
            this.pre.playNext();

        //si une pièce est sauvegardé
        }else{

            //Récupération de l'indice de la case qui sert d'ancre pour la figure sauvegardée et la figure en cours
            int nEnCours = StaticValue.getNumBlockAnchor( this.pre.partie.enCours().getData().getStyle() );
            int nSave = StaticValue.getNumBlockAnchor( this.pre.partie.getSave().getData().getStyle() );

            //Récupération des coordonnées des blocks de la figure en cours, et de la figure sauvegardé s'il elle devait être placé initialement en haut du plateau
            int[][] blockSave = StaticValue.getCoordonatInit( this.pre.partie.getSave().getData() );
            int[][] blockEnCours = this.pre.partie.enCours().getOccupedCube();

            //Calcul de l'écart des coordonées des blocks ancre pour obtenir le décalge en X et en Y a effectuer pour placer la figure placé intialement en haut du plateau à la place de la figure sauvegardé
            int diffLigne = blockEnCours[nEnCours][0] - blockSave[nSave][0];
            int diffCol = blockEnCours[nEnCours][1] - blockSave[nSave][1];

            //Vérification que la nouvelle figure en cours reste sur le plateau
            boolean peuxSwitch = true;
            int[][] blocks = this.pre.partie.enCours().getOccupedCube();
            for ( int i = 0 ; i < blocks.length ; i++ ) {
                //Ajout de la différence de coordonées aux coordonnées des blocks de la nouvelle figure pour la déplacer à la place de m'ancienne figure en cours
                int x = blockSave[i][0] + diffLigne;
                int y = blockSave[i][1] + diffCol;
                if ( x >= this.pre.partie.getBoard().length || x < 0 || y >= this.pre.partie.getBoard()[i].length || y < 0) {
                    peuxSwitch = false;
                }


            }

            //si placer la pièce sauvegardé sur le plateau ne fait pas sortir la pièce du plateau de jeu
            if ( peuxSwitch ) {
                //Sauvergarde de la nouvelle piéce sauvegardé
                Figure theSave = this.pre.partie.getSave();
                this.pre.partie.setSave(this.pre.partie.enCours());

                //Suppression sur le plateau des blocs de l'ancienne figure en cours
                for ( int i = 0 ; i < blocks.length ; i++ ) {
                    this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]] = null;
                    this.pre.setColor(blocks[i][0],blocks[i][1]);

                    //Ajout de la différence de coordonées aux coordonnées des blocks de la nouvelle figure pour la déplacer à la place de m'ancienne figure en cours
                    blockSave[i][0] += diffLigne;
                    blockSave[i][1] += diffCol;
                }

                //Sauvegarde des nouvelles coordonées de la nouvelle piece en cours, et modification pour inclure cette nouvelle piece
                theSave.setCoordonateBlock(blockSave);
                this.pre.partie.setEnCours(theSave);

                //ajouter la nouvelle pièce en cours sur le plateau de jeu
                theSave.include(this.pre);
                //afficher sur la grille de bouton représentant le plateau de jeu, la figure en cours
                theSave.updateBlockColor(this.pre);
                this.pre.updateSaveBlock();
                this.pre.reframe();
                //System.out.println("what"+this.pre.partie.toStringBoard());
            }
        }
    }


    /**
     * Méthode pour pivoter la piéce en cours de 90° dans le sdaum
     **/
    public void rotate () {
        Figure fig = this.pre.partie.enCours();
        int[][] newBlock = fig.getRotateCoord();

        //si la figure n'est pas bloqué lorsque qu'elle tourne
        if ( this.pre.partie.isNotFreeze(newBlock) ) {

            fig.getData().setRota(fig.getNextRotation());
            int[][] blocks = fig.getOccupedCube();
            //extraction de la figure en cours du tableau de jeu
            for ( int i = 0 ; i < blocks.length ; i++ ) {
                this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]] = null;
                this.pre.setColor(blocks[i][0],blocks[i][1]);
            }
            this.pre.partie.enCours().setCoordonateBlock(newBlock);
            //ajout de la figure sur le plateau
            fig.include(this.pre);
            //mise a jour des cases devant affiché les blocks de la figure
            fig.updateBlockColor(this.pre);

        }
        //System.out.println(this.pre.partie.toStringBoard());
    }



    /**
     * Méthode pour déplacer la figure en cours d'un bloc sur la gauche
     **/
    public void moveLeft () {
        //récupération de la figure en cours ainsi que des coordonées de ses blocks qui la compose
        Figure fig = this.pre.partie.enCours();
        int[][] blocks = fig.getOccupedCube();

        //boolean pour témoigner si déplacer la figure sur la gauche ne la fait pas sortir du plateau de jeu, ou qu'elle ne chavauche pas une autre pièce.
        boolean peuxBouger = true;

        //pour chaque block
        for ( int i = 0 ; i < blocks.length && peuxBouger ; i++ ) {
            //si le block est collé au bord gauche du plateau, ou si le block à sa gauche est occupé par un block n'appartenant pas à la figure en cours
            if ( blocks[i][1] <= 0 || (this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]-1] != null && !this.pre.partie.isEnCoursCase(blocks[i][0],blocks[i][1]-1)) ) {
                peuxBouger = false;
            }
        }

        //si le déplacement de la figure sur la gauche est possible selon les règles
        if ( peuxBouger ) {

            //extraction de la figure en cours du tableau de jeu
            for ( int i = 0 ; i < blocks.length ; i++ ) {
                this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]] = null;
                this.pre.setColor(blocks[i][0],blocks[i][1]);
            }

            //mise à jour des nouvelles coordonnées de la figure
            //déplacement de une case vers la gauche
            fig.setYHori(-1);
            //ajout de la figure sur le plateau
            fig.include(this.pre);
            //mise a jour des cases devant affiché les blocks de la figure
            fig.updateBlockColor(this.pre);

            //System.out.println(this.pre.partie.toStringBoard());

        }
        //appliquer les changements visuels sur la fenêtre de l'application
        this.pre.reframe();
    }




    /**
     * Méthode pour déplacer la figure en cours d'un bloc sur la droite
     **/
    public void moveRight () {
        //récupération de la figure en cours ainsi que des coordonées de ses blocks qui la compose
        Figure fig = this.pre.partie.enCours();
        int[][] blocks = fig.getOccupedCube();

        //boolean pour témoigner si déplacer la figure sur la gauche ne la fait pas sortir du plateau de jeu, ou qu'elle ne chavauche pas une autre pièce.
        boolean peuxBouger = true;

        //pour chaque block
        for ( int i = 0 ; i < blocks.length && peuxBouger ; i++ ) {

            //si le block est collé au bord droit du plateau, ou si le block à sa droite est occupé par un block n'appartenant pas à la figure en cours
            if ( blocks[i][1] >= this.pre.partie.getBoard()[0].length-1 || (this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]+1] != null && !this.pre.partie.isEnCoursCase(blocks[i][0],blocks[i][1]+1)) ) {
                peuxBouger = false;
            }
        }

        //si le déplacement de la figure sur la droite est possible selon les règles
        if ( peuxBouger ) {

            //extraction de la figure en cours du tableau de jeu
            for ( int i = 0 ; i < blocks.length ; i++ ) {
                this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]] = null;
                this.pre.setColor(blocks[i][0],blocks[i][1]);
            }

            //mise à jour des nouvelles coordonnées de la figure
            //déplacement de une case vers la droite
            fig.setYHori(1);
            //ajout de la figure sur le plateau
            fig.include(this.pre);
            //mise a jour des cases devant affiché les blocks de la figure
            fig.updateBlockColor(this.pre);

            //System.out.println(this.pre.partie.toStringBoard());

        }
        //appliquer les changements visuels sur la fenêtre de l'application
        this.pre.reframe();
    }



    /**
     * Méthode pour déplacer la figure en cours d'un bloc vers le bas
     **/
    public void moveDown () {
        //récupération de la figure en cours ainsi que des coordonées de ses blocks qui la compose
        Figure fig = this.pre.partie.enCours();
        int[][] blocks = fig.getOccupedCube();

        //boolean pour témoigner si déplacer la figure sur la gauche ne la fait pas sortir du plateau de jeu, ou qu'elle ne chavauche pas une autre pièce.
        boolean peuxBouger = true;

        //pour chaque block
        for ( int i = 0 ; i < blocks.length && peuxBouger ; i++ ) {

            //si le block est collé au bord bas du plateau, ou si le block sous lui est occupé par un block n'appartenant pas à la figure en cours
            if ( blocks[i][0] >= this.pre.partie.getBoard().length-1 || (this.pre.partie.getBoard()[blocks[i][0]+1][blocks[i][1]] != null && !this.pre.partie.isEnCoursCase(blocks[i][0]+1,blocks[i][1])) ) {
                peuxBouger = false;
            }
        }

        //si le déplacement de la figure vers le bas est possible selon les règles
        if ( peuxBouger ) {

            //extraction de la figure en cours du tableau de jeu
            for ( int i = 0 ; i < blocks.length ; i++ ) {
                this.pre.partie.getBoard()[blocks[i][0]][blocks[i][1]] = null;
                this.pre.setColor(blocks[i][0],blocks[i][1]);
            }

            //mise à jour des nouvelles coordonnées de la figure
            //déplacement de une case vers la gauche
            fig.setXVerti(1);
            //ajout de la figure sur le plateau
            fig.include(this.pre);
            //mise a jour des cases devant affiché les blocks de la figure
            fig.updateBlockColor(this.pre);

            //System.out.println(this.pre.partie.toStringBoard());

        //sinon la pièce est bloqué, on passe donc à la prochaine figure à jouer pour continuer le jeu
        }else{
            this.pre.playNext();
        }

        //appliquer les changements visuels sur la fenêtre de l'application
        this.pre.reframe();
    }
}
