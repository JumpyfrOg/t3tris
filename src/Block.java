/**
 * Classe qui représente une case ou black qui constitue une partie d'une figure. Un ensemble de block forment une figure.
 * Elle possède deux attribut x et y qui sont ses coordonnées sur le tableau qui représente le tableau de jeu.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 */
public class Block {

    //La coordonnée x, soit la ligne.
    private int x;

    //La coordonnée y, soit la colone.
    private int y;

    //oui x et y sont inversé selon un plan orthogonal

    //La figure dont elle contribut à représenter
    private Figure fig;


    /**
     * Constructeur de la classe qui demande les coordonnées que possèdera le nouveau block ainsi que la figure qu'il doit représenter
     * @param x La coordonnée x du nouveau block
     * @param y La coordonnée y du nouveau block
     * @param fig La figure que le block doit représenter
     */
    public Block ( int x , int y , Figure fig ) {
        this.x = x;
        this.y = y;
        this.fig = fig;
    }


    /**
     * Méthode pour vérifier si le block passer en paramètre  à les même coordonnées que le bloc appelé (par la méthode)
     * @param b Le block à comparer les coordonées avec cet objet.
     * @return True si les deux blocks ont les même coordonnées, False sinon.
     **/
    public boolean equals ( Block b ) {
        boolean ret = false;
        if ( this.x == b.getX() && this.y == b.getY() ) {
            ret = true;
        }
        return ret;
    }


    //Getteur et setteurs de la classe.
    public int getX () {return this.x;}
    public int getY () {return this.y;}
    public Figure getFigure () {return this.fig;}
    public void setX (int x) {this.x = x;}
    public void setY (int y) {this.y = y;}
    public void setYHori (int y) {this.y += y;}
    public void setXVerti (int x) {this.x += x;}



}
