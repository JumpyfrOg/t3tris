import javax.swing.*;
import java.awt.event.*;

/**
 * Classe servant de chronometre pour le jeu. Elle contient un timer d'intervalle de 1s pour chaque ActionEvent lancé. Ces ActionEvents
 * sont intercepté par cette classe qui va faire automatiquement descendre la piece joué en cours sur le plateau d'une case en executant
 * la methode "moveDown" de la classe ListenerKey pour simuler l'action de vouloir descendre le pièce.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 */
public class Chrono implements ActionListener {

    //Le timer du jeu
    private Timer tim;

    //Le listener du jeu, la classe qui interprète les actions de l'utilisateur
    private ListenerKey list;

    /**
     * Constructeur de la classe Chrono demandant le timer a surveiller et le le listener qui s'occupe des actions humaines
     * @param tim Le timer qui génére des ActionEvent
     * @param list La classe qui réagit aux actions humaines
     */
    public Chrono (Timer tim, ListenerKey list) {
        super();
        this.tim = tim;
        this.list = list;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.list.moveDown();
    }

    /**
     * Méthode pour arrêter le chronometre de la partie.
     */
    public void stop () {
        this.tim.stop();
    }

    /**
     * Méthode pour réanitialiser le timer et ensuite le lancer
     */
    public void reInit () {
        this.tim = new Timer(1000,this);
        this.tim.start();
    }

    /**
     * Méthode pour lancer ou relancer le timer
     */
    public void start () {
        this.tim.start();
    }

}
