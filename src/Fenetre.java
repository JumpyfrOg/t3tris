import javax.swing.*;

/**
 * Cette classe est la fenêtre du jeu. Elle va directement afficher l'ath du jeu.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class Fenetre extends JFrame {

    //Le chemin qui sépare le lieu de l'exécution du jeu du dossier contenant les données du jeu
    public String dataSource;

    //La classe qui possède le même rôle que la classe "présentation" dans le patron de conception MVP. Cette classe contient la logique métier de l'application, c'est elle qui va réagir aux actions de l'utilisateur.
    private Presentation p;


    /**
     * Constructeur de la classe et donc de la fenêtre de jeu. Cette méthode va initialiser l'attribut dataSource de la classe et appeler la méthode initComponents.
     * @param dataSource Le chemin qui sépare le lieu de l'exécution du jeu du dossier contenant les données du jeu
     */
    public Fenetre ( String dataSource ) {
        super("T3tris - 0 pts");

        this.initComponents();
        this.p = new Presentation(this,dataSource);
    }


    /**
     * Méthode qui va initialiser la fenêtre de jeu pour ses différents paramètre comme la taille
     */
    public void initComponents() {
        //200 panel gauche - 450 middle - 200 panel droite
        this.setSize(758,760);
        this.setLocationRelativeTo(null);
		    this.setResizable(false);
		    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setFocusable(true);
        this.setFocusTraversalKeysEnabled(false);
    }




    /**
	 * Méthode lanceuse du jeu. C'est la méthode qui va s'exécuter en premier pour l'exécution du jeu.
	 * Cette méthode va créer une classe fenetre et lui donner le chemin qui sépare le lieu de l'exécution du dossier data
	 * @param args La chaine de caractère saisie suivant la commande pour l'exécution de du jeu
	 **/
	public static void main ( String [] args ) {
		SwingUtilities.invokeLater(new Runnable() {
      		public void run() {

				//Le chemin à choisir
				String dataSource = "../data/";
				//String dataSource = "data/";

				new Fenetre(dataSource);
      		}
    	});
    }

}
