# **T3tris**

[![forthebadge](https://forthebadge.com/images/badges/made-with-java.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)

Little Tetris done on my spare time. This Tetris is played on keyboard to control the pieces played.

<br></br>
<br></br>

# Application

The application allows to play to the Tetris game, as long as the game isn't lost. It contains a timer of the game time at the bottom of the window, the score is displayed on the window title. It's possible to save a piece (displayed at the left top) to then switch it with the piece/figure that you are playing on the board. The four next  pieces/figures to play are displayed in order of appearance, top to bottom.
![image de l'interface](https://gitlab.com/JumpyfrOg/t3tris/-/raw/master/readme_picture/SreenShotForDemo.png)
<br></br>
<br></br>
# Installation and start

*The application dont need a specific instalation. You must have the [JRE 1.8 (JRE 8)](https://www.oracle.com/java/technologies/javase-jre8-downloads.html) to run the application, however your computer should have it by default, otherwise refer to a tutorial. After clone this project, create a folder "ws" à the root (in the same folder where the foldres "class" and "src" are present). All commands mentioned in this part will be considered as executed from the folder "ws".*

<br></br>
 - Compile the application files (if you have made any changes) :

The project was develop with the [JDK 1.8.0_60 (JDK 8)](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html). Refer to a tutorial in order to install the development environment of java. Then, executed the command `javac -d ../class ../src/*` to compile java files from the "src" folder and store the binary files generated in the folder "class".

<br></br>
 - Run the application :

 &rarr; Either enter the command `java Fenetre`.

 &rarr;  Or run the "T3tris.jar" archive (the "data" folder must be placed in the same folder that the archive)

<br></br>
<br></br>
# Game control
![commande du jeu](https://gitlab.com/JumpyfrOg/t3tris/-/raw/master/readme_picture/clavier.png)Les différentes touches fonctionnelles pour jouer à T3tris selon votre type de profil gamer.
The differents functionnal keys to play to T3tris acording your gamer profil.

- Key light blue: move the piece that you control (to the left, right or bottom)
- Key dark blue : rotate the piece
- Key green : switch the piece controled with the saved piece

<br></br>
<br></br>
# Software used

- Online photo editor : [Pixlr](https://pixlr.com/)
- IDE : [Visual Studio Code](https://code.visualstudio.com/) / [Atom](https://atom.io/)
- Online markdown editor : [Stackedit](https://stackedit.io/)


<br></br>
# Contact

- Mail : mcharron29@gmail.com
- Twitter : [Charron M](https://twitter.com/CharronM5)
