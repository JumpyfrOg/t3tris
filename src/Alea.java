import java.awt.*;

/**
 * Classe asbtraite qui va s'occuper de tout ce qui aléatoire dans le programme comme la génération de figure, le choix de sa couleur etc...
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public abstract class Alea {

    /**
     * Méthode pour choisir aléatoirement une rotation. Un chiffre est choisi aléatoirement, et selon ce chiffre, la rotation est choisie
     * @return La rotation choisi aléatoirement
     */
    public static Rotation RotaAuto () {
        Rotation rota = null;
        //Choix alléatoire d'un chiffre
        int index = (int) Math.round(Math.random()*4);

        //Pour 0, rotation initiale, haut. Pour 1, rotation de 90°, droite. Pour 2, rotation de 180°, bas. Pour 3, rotation de -90°, gauche.
        if ( index < 1 ) {
            rota = Rotation.haut;
        }else if ( index >= 1 && index < 2 ) {
            rota = Rotation.droite;
        }else if ( index >= 2 && index < 3 ) {
            rota = Rotation.bas;
        }else if ( index >= 3 ) {
            rota = Rotation.gauche;
        }
        //System.out.println("Rota 0-3: "+index);
        return rota;
    }




    /**
     * Méthode pour choisir aléatoirement un style. Un chiffre est choisi aléatoirement, et selon ce chiffre, le style est choisi
     * @return La style choisi aléatoirement
     **/
    public static Style StyleAuto () {
        Style style = null;
        //Choix alléatoire d'un chiffre
        int index = (int) Math.round(Math.random()*7);

        /*
        Liste des styles selon le chiffre choisi aléatoirement
        0- barre (de 0 à 0.99)
        1- l_bas_droit (de 1 à 1.99)
        2- l_bas_gauche (...)
        3- carre
        4- triangle
        5- z_bas_droit
        6- z_bas_gauche
        */

        if ( index < 1 ) {
            style = Style.barre;
        }else if ( index >= 1 && index < 2 ) {
            style = Style.l_bas_droit;
        }else if ( index >= 2 && index < 3 ) {
            style = Style.l_bas_gauche;
        }else if ( index >= 3 && index < 4 ) {
            style = Style.carre;
        }else if ( index >= 4 && index < 5 ) {
            style = Style.triangle;
        }else if ( index >= 5 && index < 6 ) {
            style = Style.z_bas_droit;
        }else if ( index >= 6 ) {
            style = Style.z_bas_gauche;
        }
        //System.out.println("Style 0-6: "+index);
        return style;
    }




    /**
     * Méthode pour choisir aléatoirement une couleur. Un chiffre est choisi aléatoirement, et selon ce chiffre, la couleur est choisie.
     * @return La couleur choisi aléatoirement.
     */
    public static Color ColorAuto () {
        Color color = null;
        //Choix alléatoire d'un chiffre
        int index = (int) Math.round(Math.random()*7);

        /*
        Liste des couleurs selon le chiffre choisi aléatoirement
        0- blue
        1- cyan
        2- green
        3- orange
        4- magenta
        5- red
        6- yellow
        */

        if ( index < 1 ) {
            color = Color.blue;
        }else if ( index >= 1 && index < 2 ) {
            color = Color.cyan;
        }else if ( index >= 2 && index < 3 ) {
            color = Color.green;
        }else if ( index >= 3 && index < 4 ) {
            color = Color.orange;
        }else if ( index >= 4 && index < 5 ) {
            color = Color.magenta;
        }else if ( index >= 5 && index < 6 ) {
            color = Color.red;
        }else if ( index >= 6 ) {
            color = Color.yellow;
        }
        //System.out.println("Couleur 0-6: "+index);
        return color;
    }

}
