/**
 * Cette classe représente une figure qui apparaîtra en haut de la grille est qui decendra d'une case toute les secondes
 * jusqu'à ce qu'il soit bloqué par d'autre figure. Une figure est constitué d'un tableau de block qui ensemble forme la figure. Elle contient également un objet
 * FigureMeta qui contient les différentes informations de la figure.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class Figure {

    //Le tableau de block qui forme la figure
    private Block[] fig;

    //Les données de la figure
    private FigureMeta data;



    //Les différents constructeurs de la classe

    /**
     * Constructeur de la classe Figure pour générer une figure en fesant passer ses données par paramètre, et la liste de coordonnées des blocks qui constitue la figure.
     * @param data Les données de la Figure
     * @param caseOccuped La liste de coordonnées des blocks qui constitue la figure.
     */
    public Figure ( FigureMeta data , int[][] caseOccuped ) {
        this.data = data;
        this.fig = new Block[4];
        if ( caseOccuped != null ) {
            //créer et sauver des blocks aux coordonnées passé par paremètre
            this.fill(caseOccuped);
        }

    }

    /**
     * Constructeur de la classe Figure pour générer une figure avec la liste de coordonnées des blocks qui constitue la figure passé par paramètre. Cependant les données de la figure sont généré aléatoirement par la méthode FigureMeta.FigureMeteAuto()
     * @param caseOccuped La liste de coordonnées des blocks qui constitue la figure.
     */
    public Figure ( int[][] caseOccuped ) {
        this.data = FigureMeta.FigureMetaAuto();
        this.fig = new Block[4];
        //créer et sauver des blocks aux coordonnées passé par paremètre
        this.fill(caseOccuped);
    }


    /**
     * Constructeur de la classe Figure pour générer une figure avec la liste de coordonnées des blocks pour placer la figure au milieu haut du tableau, comme si la figure venait d'arriver. Cependant les données de la figure sont généré aléatoirement par la méthode FigureMeta.FigureMeteAuto()
     */
    public Figure () {
        this.data = FigureMeta.FigureMetaAuto();
        this.fig = new Block[4];
        //créer et sauvé des blocks au coordonnées par défaut en haut du plateau.
        this.fillInit();
    }







    /**
     * Methode pour récupérer la liste des coordonnées actuelles des blocks qui constituent la figure. Chaque ligne représente un block, la colone 0 contient la ligne et la colone 1 la colone servant de coordonées pour le bloc.
     * @return La liste des coordonées des blocks de la figure.
     */
    public int[][] getOccupedCube () {
        int[][] ret = new int[this.fig.length][2];
        for ( int i = 0 ; i < this.fig.length ; i++ ) {
            ret[i][0] = this.fig[i].getX();
            ret[i][1] = this.fig[i].getY();
        }
        return ret;
    }


    /**
     * Methode pour remplir le tableau fig, attribut de cette classe. Pour chaque cellule du tableau fig, des objets blocks vont être crées et placé dans la cellule.
     * Le tableau passé par paramètre contient les coordonnées à fournir à la création des objets blocks pour les placé sur le plateau de jeu.
     * @param caseOccuped Le tableau de coordonnées des blocks à générer.
     */
    public void fill ( int[][] caseOccuped ) {
        if ( this.fig != null ) {
            for ( int i = 0 ; i < this.fig.length ; i++ ) {
                this.fig[i] = new Block(caseOccuped[i][0],caseOccuped[i][1],this);
            }
        }
    }


    /**
     * Methode pour remplir le tableau fig, attribut de cette classe. Pour chaque cellule du tableau fig, des objets blocks vont être crées et placé dans la cellule.
     * Les coordonnées à fournir à la création des objets blocks, pour les placer sur le plateau de jeu, sont récupéré via la classe StaticValue afin d'obtenir les coordonnées par défaut selon le style de la figure et sa rotation.
     */
    public void fillInit () {
        if ( this.fig != null ) {
            //récupérer les coordonnées par défaut selon les méta-données de cette figure.
            int[][] coor = StaticValue.getCoordonatInit(this.data);
            for ( int i = 0 ; i < this.fig.length ; i++ ) {
                this.fig[i] = new Block(coor[i][0],coor[i][1],this);
            }
        }
    }


    /**
     * Méthode pour ajouter la figure sur le tableau de jeu, en ajoutant chacun des ses blocks
     * @param pre L'objet qui lie la fenêtre et les données.
     */
    public void include ( Presentation pre ) {
        if ( this.fig != null ) {
            for ( int i = 0 ; i < this.fig.length ; i++ ) {
                pre.partie.getBoard()[this.fig[i].getX()][this.fig[i].getY()] = this.fig[i];
            }
        }
    }


    /**
     * Méthode pour mettre a jour les boutons représentant les cases qu'utilise la figure, afin de faire afficher sur la fenetre la figure.
     * @param pre L'objet qui lie la fenêtre et les données.
     */
    public void updateBlockColor ( Presentation pre ) {
        if ( this.fig != null ) {
            for ( int i = 0 ; i < this.fig.length ; i++ ) {
                pre.setColor(this.fig[i].getX(),this.fig[i].getY());
            }
        }
    }

    /**
     *
     *
     *
    public boolean hasTwice () {
        boolean ret = false;
        int i = 0;
        while ( i < this.fig.length && !ret ) {
            int j = i + 1;
            while ( j < this.fig.length && !ret ) {
                if ( this.fig[i].equals(this.fig[j]) ) {
                    ret = true;
                }
                j++;
            }
            i++;
        }
        return ret;
    }*/



    /**
     * Méthode pour attibuer des coordonnées aux blocks de la figure
     * @param newBlocks Les coordonnées des nouveaux blocks
     */
    public void setCoordonateBlock ( int [][] newBlocks ) {
        for ( int i = 0 ; i < newBlocks.length ; i++ ) {
            this.fig[i].setX(newBlocks[i][0]);
            this.fig[i].setY(newBlocks[i][1]);
        }

    }


    /**
     * Méthode pour récupérer les coordonées des blocks de la figure si elle devait être pivoter
     * @return Le tableau de coordonées des blocks si la figure est pivoté
     */
    public int[][] getRotateCoord () {

        int[][] ret = this.getOccupedCube();

        //Barre
        if ( this.data.getStyle() == Style.barre ) {

            //Horizontal
            if ( this.data.getRota() == Rotation.gauche || this.data.getRota() == Rotation.droite ) {

                //première case
                ret[0][0] = ret[0][0] - 2;
                ret[0][1] = ret[0][1] + 2;

                //deuxième case
                ret[1][0] = ret[1][0] - 1;
                ret[1][1] = ret[1][1] + 1;

                //troisime case, ne bouge pas
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] + 1;
                ret[3][1] = ret[3][1] - 1;

            //vertical
            }else if ( this.data.getRota() == Rotation.haut || this.data.getRota() == Rotation.bas ) {

                //première case
                ret[0][0] = ret[0][0] + 2;
                ret[0][1] = ret[0][1] - 2;

                //deuxième case
                ret[1][0] = ret[1][0] + 1;
                ret[1][1] = ret[1][1] - 1;

                //troisime case, ne bouge pas
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] - 1;
                ret[3][1] = ret[3][1] + 1;
            }
        }






        //L bas droit
        if ( this.data.getStyle() == Style.l_bas_droit ) {

            //debout - haut
            if ( this.data.getRota() == Rotation.haut ) {
                //rotation "droite" de 90°

                //première case
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] - 1;
                ret[2][1] = ret[2][1] - 1;

                //quatrième case
                ret[3][0] = ret[3][0];
                ret[3][1] = ret[3][1] - 2;


            // |---, L tourné de 90° dans le sens des aum
            }else if ( this.data.getRota() == Rotation.droite ) {
                //rotation venant à 180° de la base

                //première case
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] - 1;
                ret[2][1] = ret[2][1] + 1;

                //quatrième case
                ret[3][0] = ret[3][0] - 2;
                ret[3][1] = ret[3][1];


            // -|, L inverser par ligne horizontal
            }else if ( this.data.getRota() == Rotation.bas ) {
                //rotation venant à -90° de la base

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] + 1;
                ret[2][1] = ret[2][1] + 1;

                //quatrième case
                ret[3][0] = ret[3][0];
                ret[3][1] = ret[3][1] + 2;

            // ___|, L inverser par ligne horizontal
            }else if ( this.data.getRota() == Rotation.gauche ) {
                //rotation venant à la base

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] + 1;
                ret[2][1] = ret[2][1] - 1;

                //quatrième case
                ret[3][0] = ret[3][0] + 2;
                ret[3][1] = ret[3][1];
            }
        }







        //L bas gauche -> _|
        if ( this.data.getStyle() == Style.l_bas_gauche ) {

            //debout - haut
            if ( this.data.getRota() == Rotation.haut ) {
                //rotation "droite" de 90°

                //première case
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] - 1;
                ret[2][1] = ret[2][1] - 1;

                //quatrième case
                ret[3][0] = ret[3][0] - 2;
                ret[3][1] = ret[3][1];


            // |___, L tourné de 90° dans le sens des aum
            }else if ( this.data.getRota() == Rotation.droite ) {
                //rotation venant à 180° de la base

                //première case
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] - 1;
                ret[2][1] = ret[2][1] + 1;

                //quatrième case
                ret[3][0] = ret[3][0];
                ret[3][1] = ret[3][1] + 2;


            // |-, L inverser par ligne horizontal
            }else if ( this.data.getRota() == Rotation.bas ) {
                //rotation venant à -90° de la base

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] + 1;
                ret[2][1] = ret[2][1] + 1;

                //quatrième case
                ret[3][0] = ret[3][0] + 2;
                ret[3][1] = ret[3][1];

            // ---|, L inverser par ligne horizontal
            }else if ( this.data.getRota() == Rotation.gauche ) {
                //rotation venant à la base

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] + 1;
                ret[2][1] = ret[2][1] - 1;

                //quatrième case
                ret[3][0] = ret[3][0];
                ret[3][1] = ret[3][1] - 2;
            }
        }






        //Triangle
        if ( this.data.getStyle() == Style.triangle ) {

            //debout - pointe vers le haut  _|_
            if ( this.data.getRota() == Rotation.haut ) {
                //rotation "droite" de 90°

                //première case (la pointe)
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0] - 1;
                ret[1][1] = ret[1][1] + 1;

                //troisime case
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] + 1;
                ret[3][1] = ret[3][1] - 1;


            // |-, triangle tourné de 90° dans le sens des aum
            }else if ( this.data.getRota() == Rotation.droite ) {
                //rotation venant à 180° de la base

                //première case
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0] + 1;
                ret[1][1] = ret[1][1] + 1;

                //troisime case
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] - 1;
                ret[3][1] = ret[3][1] - 1;


            // -;-, triangle inversé par ligne horizontal
            }else if ( this.data.getRota() == Rotation.bas ) {
                //rotation venant à -90° de la base

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0] + 1;
                ret[1][1] = ret[1][1] - 1;

                //troisime case
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] - 1;
                ret[3][1] = ret[3][1] + 1;

            // -|
            }else if ( this.data.getRota() == Rotation.gauche ) {
                //rotation venant à la base

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0] - 1;
                ret[1][1] = ret[1][1] - 1;

                //troisime case
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] + 1;
                ret[3][1] = ret[3][1] + 1;
            }
        }





        //z bas droit
        if ( this.data.getStyle() == Style.z_bas_droit )  {

            //axe du milieu horizontal
            if ( this.data.getRota() == Rotation.haut || this.data.getRota() == Rotation.bas ) {

                //première case
                ret[0][0] = ret[0][0];
                ret[0][1] = ret[0][1] + 2;

                //deuxième case
                ret[1][0] = ret[1][0] - 1;
                ret[1][1] = ret[1][1] + 1;

                //troisime case
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] - 1;
                ret[3][1] = ret[3][1] - 1;

            //axe du milieu vertical
            }else if ( this.data.getRota() == Rotation.droite || this.data.getRota() == Rotation.gauche) {

                //première case
                ret[0][0] = ret[0][0];
                ret[0][1] = ret[0][1] - 2;

                //deuxième case
                ret[1][0] = ret[1][0] + 1;
                ret[1][1] = ret[1][1] - 1;

                //troisime case
                ret[2][0] = ret[2][0];
                ret[2][1] = ret[2][1];

                //quatrième case
                ret[3][0] = ret[3][0] + 1;
                ret[3][1] = ret[3][1] + 1;
            }
        }





        //z bas gauche
        if ( this.data.getStyle() == Style.z_bas_gauche )  {

            //axe du milieu horizontal
            if ( this.data.getRota() == Rotation.haut || this.data.getRota() == Rotation.bas ) {

                //première case
                ret[0][0] = ret[0][0] + 1;
                ret[0][1] = ret[0][1] + 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] - 1;
                ret[2][1] = ret[2][1] + 1;

                //quatrième case
                ret[3][0] = ret[3][0] - 2;
                ret[3][1] = ret[3][1];

            //axe du milieu vertical
            }else if ( this.data.getRota() == Rotation.droite || this.data.getRota() == Rotation.gauche) {

                //première case
                ret[0][0] = ret[0][0] - 1;
                ret[0][1] = ret[0][1] - 1;

                //deuxième case
                ret[1][0] = ret[1][0];
                ret[1][1] = ret[1][1];

                //troisime case
                ret[2][0] = ret[2][0] + 1;
                ret[2][1] = ret[2][1] - 1;

                //quatrième case
                ret[3][0] = ret[3][0] + 2;
                ret[3][1] = ret[3][1];
            }
        }


        //System.out.println(" -> "+java.util.Arrays.deepToString(ret));
        return ret;
    }


    /**
     * Méthode qui retourne la prochaine rotation de la figure
     * @return La prochaine rotation de la figure
     */
    public Rotation getNextRotation () {
        Rotation ret = null;
         //Barre
         if ( this.data.getStyle() == Style.barre ) {

            //Horizontal
            if ( this.data.getRota() == Rotation.gauche || this.data.getRota() == Rotation.droite ) {

                ret = Rotation.haut;

            //vertical
            }else if ( this.data.getRota() == Rotation.haut || this.data.getRota() == Rotation.bas) {

                ret = Rotation.droite;

            }
        }else



        //L bas droit
        if ( this.data.getStyle() == Style.l_bas_droit ) {

            //debout - haut
            if ( this.data.getRota() == Rotation.haut ) {
                //rotation "droite" de 90°

                ret = Rotation.droite;

            // |---, L tourné de 90° dans le sens des aum
            }else if ( this.data.getRota() == Rotation.droite ) {
                //rotation venant à 180° de la base

                ret = Rotation.bas;

            // -|, L inverser par ligne horizontal
            }else if ( this.data.getRota() == Rotation.bas ) {
                //rotation venant à -90° de la base

                ret = Rotation.gauche;

            // ___|, L inverser par ligne horizontal
            }else if ( this.data.getRota() == Rotation.gauche ) {
                //rotation venant à la base

                ret = Rotation.haut;

            }
        }else


        //L bas gauche -> _|
        if ( this.data.getStyle() == Style.l_bas_gauche ) {

            //debout - haut
            if ( this.data.getRota() == Rotation.haut ) {
                //rotation "droite" de 90°

                ret = Rotation.droite;

            }else if ( this.data.getRota() == Rotation.droite ) {
                //rotation venant à 180° de la base

                ret = Rotation.bas;

            }else if ( this.data.getRota() == Rotation.bas ) {
                //rotation venant à -90° de la base

                ret = Rotation.gauche;

            }else if ( this.data.getRota() == Rotation.gauche ) {
                //rotation venant à la base

                ret = Rotation.haut;

            }
        }else






        //Triangle
        if ( this.data.getStyle() == Style.triangle ) {

            //debout - haut
            if ( this.data.getRota() == Rotation.haut ) {
                //rotation "droite" de 90°

                ret = Rotation.droite;

            }else if ( this.data.getRota() == Rotation.droite ) {
                //rotation venant à 180° de la base

                ret = Rotation.bas;

            }else if ( this.data.getRota() == Rotation.bas ) {
                //rotation venant à -90° de la base

                ret = Rotation.gauche;

            }else if ( this.data.getRota() == Rotation.gauche ) {
                //rotation venant à la base

                ret = Rotation.haut;

            }
        }else





        //z bas droit
        if ( this.data.getStyle() == Style.z_bas_droit )  {

            //axe du milieu horizontal
            if ( this.data.getRota() == Rotation.haut || this.data.getRota() == Rotation.bas ) {

                ret = Rotation.droite;

            //axe du milieu vertical
            }else if ( this.data.getRota() == Rotation.droite || this.data.getRota() == Rotation.gauche) {

                ret = Rotation.haut;

            }
        }else





        //z bas gauche
        if ( this.data.getStyle() == Style.z_bas_gauche )  {

            //axe du milieu horizontal
            if ( this.data.getRota() == Rotation.haut || this.data.getRota() == Rotation.bas ) {

                ret = Rotation.droite;

            //axe du milieu vertical
            }else if ( this.data.getRota() == Rotation.droite || this.data.getRota() == Rotation.gauche) {

                ret = Rotation.haut;

            }
        }

        return ret;
    }





    /**
     * Méthode qui va appliquer pour toutes les blocks de la figure le décalage horizontal sur le tableau selon la valeur passée par paramètre.
     * La valeur passée en paramètre est le décalge horizontal par rapport à la position du block, un chiffre positif fait décaler les blocks sur la droite, gauche s'il est négatif.
     * @param deplacement Le décalage horizontal (en nombre de case) de la figure par rapport à sa possition actuelle
     */
    public void setYHori (int deplacement ) {
        for ( int i = 0 ; i < this.fig.length ; i++ ) {
            this.fig[i].setYHori(deplacement);
        }
    }


    /**
     * Méthode qui va appliquer pour toutes les blocks de la figure le décalage vertical sur le tableau selon la valeur passée par paramètre.
     * La valeur passée en paramètre est le décalge vertical par rapport à la position du block, un chiffre positif fait décaler les blocks vers le bas, haut s'il est négatif.
     * @param deplacement Le décalage vertical (en nombre de case) de la figure par rapport à sa possition actuelle
     */
    public void setXVerti(int deplacement ) {
        for ( int i = 0 ; i < this.fig.length ; i++ ) {
            this.fig[i].setXVerti(deplacement);
        }
    }





    //Getteur et setteur de la classe
    public FigureMeta getData () {return this.data;}
    public Block[] getBlocks () {return this.fig;}

}
