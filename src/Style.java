/**
 * Enumération des styles de figure qui peuvent apparaitre dans le jeu. A chaque style, est associé le nombre de rotation qu'il peut effectuer.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public enum Style {

    barre(2),

    l_bas_droit(4),

    l_bas_gauche(4),

    carre(0),

    triangle(4),

    z_bas_droit(2),

    z_bas_gauche(2);


    //Le nombre de rotation que la figure peut effectuer
    private int nbRota;



    /**
     * Constructeur de l'énumération
     * @param nbRota Le nombre de rotation possible pour une figure
     */
    private Style ( int nbRota ) {
        //si le nombre de rotation est dans les bornes des rotations des figures du jeu
        if ( nbRota >= 0 && nbRota <= 4 ) {
            this.nbRota = nbRota;
        }
    }



    //Getteur pour récupérer le nombre de rotation du style de la figure
    public int getNbRota () {return this.nbRota;}


}
