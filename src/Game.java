/**
 * Classe qui représente la partie algorythmique du jeu, de la partie.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class Game {

    //Le tableau de jeu, contient un objetc block pour représenter un block, sinon null s'il ne contient rien.
    private Block[][] board;

    //La figure qui est entrain d'être commander par le joeuur
    private Figure enCours;

    //la liste de prochaines figures à jouer, placé dans leur ordre d'utilisation
    private Figure[] next;

    //la figure sauvegardé par le joueur
    private Figure save;

    //Le score de la partie, le remplissage d'un ligne faut 1000 points.
    private long score;

    //
    private int minute;

    //
    private int seconde;



    /**
     * Le constructeur de le classe Game qui va initialiser le plateau de jeu ainsi que la figure à jouer et les prochaines également.
     **/
    public Game () {
        this.board = new Block[20][10];
        this.score = 0;
        this.minute = 0;
        this.seconde = 0;
        this.initialiseBeginBlocks();
    }

    /**
     * Méthode pour créeer 5 figures, 1 sera celle en cours et les 4 autres seront les suivantes
     */
    public void initialiseBeginBlocks () {

        //Créer 4 nouvelles figures, les suivantes
        this.next = new Figure[4];
        for ( int i = 0 ; i < this.next.length ; i++ ) {
            this.next[i] = new Figure();
        }

        //Créer une nouvelle figure, celle qui sera controlé par le joueur
        this.enCours = new Figure();
    }


    /**
     * Méthode qui détermine si la case déterminée par les coordonnées passées par paramètre est occupée par un block appartenant à la figure en cours
     * @param ligne La ligne de la case
     * @param col La colone de la case
     * @return True si la case est occupé par un block de la case en cours, False i elle n'appartient à une autre figure ou est null soit non occupée.
     */
    public boolean isEnCoursCase(int ligne, int col) {
        boolean ret = false;
        int i = 0;
        //parcours de chaque blocks composant la figure en cours
        while ( i < this.enCours.getOccupedCube().length && !ret ) {
            //si la ligne et la colonne sont les même que le block
            if ( ligne == this.enCours.getOccupedCube()[i][0] && col == this.enCours.getOccupedCube()[i][1] ) {
                ret = true;
            }
            i++;
        }
        return ret;
    }



    /**
     * Méthode qui va parcourir le tableau pour vérifier si des lignes sont remplis pour les retirer et ajouter des points au joueur.
     */
    public void checkLine () {
        int ligne = this.board.length;
        int colone;
        //le nombre de ligne rempli afin de modifier le score
        int serie = 0;
        //booléen pour témoigner s'il faut continuer d'analyser les lignes
        boolean continuer = true;
        //booleén pour témoigner si la ligne déterminé par la valeur de la variable "ligne" doit être vidée
        boolean ligneAVider = false;

        //tant qu'il faut continuer
        while ( continuer ) {

            //nouvelle ligne a check, la ligne de audessus
            ligne--;

            //si on a déjà check toute les lignes du tableau
            if ( ligne < 0 ) {
                continuer = false;

            //sinon, checker la ligne
            }else{
                colone = this.board[ligne].length-1;
                ligneAVider = true;
                //pour chaque colone de la ligne, du moment qu'on arrive pas à la fin et que la ligne doit être vidé
                while ( ligneAVider && colone >= 0 ) {
                    //si la case de la colone est vide
                    if ( this.board[ligne][colone] == null ) {
                        //alors la ligne n'est pas à vider
                        ligneAVider = false;
                    }else{
                        //sinon continuer la vérificatiion
                        colone--;
                    }
                }

                //si la ligne est remplie, il faut l'enlever
                if ( ligneAVider ) {
                    //suppression de la ligne
                    this.removeLigne(ligne);
                    serie++;
                    //annuler le changement de ligne, comme la ligne vient d'être supprimer, la prochaine ligne à vérifié se retrouve à la même ligne puisqu'on à décalé les lignes vers le bas après la suppression
                    ligne++;
                }
            }
        }
        this.upScore(serie);
    }



    /**
     * Méthode qui va retirer la ligne dont le numéro est passé par paramètre, et va décaler les lignes d'au-dessus (numéro inférieur à cette ligne) de un block vers le bas pour combler la place laisser par cette suppresion de ligne
     * @param ligne La ligne où retirer une ligne
     */
    public void removeLigne (int ligne) {
        //booléen qui témoigne s'il faut continuer de parcourir le tableau pour décaler les lignes
        boolean continuer = true;
        //tant que la ligne à décaler n'est pas la dernière au sommet du plateau
        while ( continuer && ligne > 0 ) {
            continuer = false;
            //pour chaque case de la ligne
            for ( int i = 0 ; i < this.board[ligne].length ; i++ ) {
                //décalage de la case d'audessus sur cette ligne
                this.board[ligne][i] = this.board[ligne - 1][i];
                //si la case décaler n'est pas nul, alors il est possible qu'il reste des cases à décaler, donc on continu
                if ( this.board[ligne - 1][i] != null ) {
                    continuer = true;
                }
            }
            ligne--;

        }
    }





    /**
     * Méthode qui va regarder si les cases déterminées par les coordonnées passé par paramètre ne désignent pas de cases contenant le blocks
     * d'une figure déja joué et donc impossible de déplacer. La figure en cours peut être déplacé et n'est donc pas pris en compte.
     * Si une case sort du tableau, elle est considérée comme occupée et bloquée, la méthode renvoie donc "false".
     * @param blocks Le tableau de coordonnées à vérifié leur appartenance.
     * @return True si la case est occupé et non déplacable.
     */
    public boolean isNotFreeze ( int [][] blocks ) {
        boolean ret = true;
        for ( int i = 0 ; i < blocks.length ; i++ ) {
            //si les coordonées ne sorte pas du tableau, ou si la case est occupé et occupé par une autre piece que la pièce en cours
            if ( (blocks[i][0] < 0 || blocks[i][0] >= this.board.length || blocks[i][1] < 0 || blocks[i][1] >= this.board[i].length) || (this.board[blocks[i][0]][blocks[i][1]] != null && !this.isEnCoursCase(blocks[i][0], blocks[i][1])) ) {
                ret = false;
            }
        }
        return ret;
    }


    /**
     * Méthode qui va changer le score de la partie. En fonction du nombre de ligne rempli dû au dernier coup du joueur (nombre passé en paramètre), la méthode va calculer le nombre de points gagné et l'ajouter au score de la partie.
     * @param nbRempli Le nombre de ligne rempli d'un coup par le dernier coup effectué du joueur.
     **/
    public void upScore ( int nbRempli ) {

        //si le joueur a remplis une ligne
        if ( nbRempli > 0 ) {
            //une ligne vaut 1000 points, calcul des points obtenus pour le remplisages des lignes
            int pointLigne = 1000 * nbRempli;

            if ( nbRempli > 1 ) {
              //calcul du coeficient pour obtenir un bonus de point, une série de 2 donne 25% de points supplémentaire. Une série de 3, 50%, et une série de 4, 75%.
              double coef = (double)(nbRempli-1) / (double)4;
              int pointBonus = (int) (pointLigne * coef);
              pointLigne += pointBonus;
              //System.out.println("Point bonus: "+pointBonus+"   n: "+nbRempli);
            }
            //ajout au score actuel, les points que vient de gagner le joueur
            this.score += pointLigne;
        }
    }


    /**
     * Méthode qui va augmenter de 1 seconde le temps sauvegardé. Si le nombre de seconde sauvegardé est égale à 59, alors les secondes reviennent à 0 et les minutes sont incrémenté de 1.
     **/
    public void secondePlus () {
        if ( this.seconde == 59 ) {
            this.minute++;
            this.seconde = 0;
        }else{
            this.seconde++;
        }
    }



    /**
     * Méthode pour joueur la prochaine figure en placant la prochaine figure à jouer comme figure en cours, décaler les autres pièces à jouer, et créer une nouvelle figure pour la dernière prochaine pièce à jouer.
     */
    public void next () {
        this.enCours = this.next[0];
        this.next[0] = this.next[1];
        this.next[1] = this.next[2];
        this.next[2] = this.next[3];
        this.next[3] = new Figure();
    }

    //Getteurs et setteurs de la classe
    public Block[][] getBoard(){return this.board;}
    public Figure[] getNext(){return this.next;}
    public Figure getFigure(){return this.save;}
    public Figure enCours(){return this.enCours;}
    public void setEnCours(Figure fig){this.enCours = fig;}
    public Figure getSave(){return this.save;}
    public void setSave(Figure fig){this.save = fig;}
    public long getScore(){return this.score;}
    public int getSeconde(){return this.seconde;}
    public int getMinute(){return this.minute;}


    /**
     * Méthode qui rend le tableau de jeu sous le format d'un String. Le tableau affiche un point pour désigner une case vide, et une croix "X" pour désigner une case occupé.
     * @return Le tableau d ejeu sous le format d'un String.
     **/
    public String toStringBoard () {
        String ret = "~~~\n";
        for ( int i = 0 ; i < this.getBoard().length ; i++ ) {
            for ( int j = 0 ; j < this.getBoard()[i].length ; j++ ) {
                if ( this.board[i][j] != null ) {
                    ret += "X ";
                }else{
                    ret += ". ";
                }
            }
            ret += "\n";
        }
        return ret;
    }

}
