/**
 * Cette énumération énumère les différents sens qu'une figure peut posseder. La rotation d'une figure se détermine selon la rotation différente que la rotation d'initialisation de la figure.
 * Une rotation possède un string étant sa représention textuelle et un entier correspondant a son ordre de rotation, sachant que la rotation d'une figure se fait en pivotant le haut de la figure vers la droite.
 *
 *              /0°/
 *               |
 *     /-90°/  - O -  /90°/
 *               |
 *             /180°/
 *
 *                                              __
 * 0°/   |           90°/                180°/    |       -90°/
 * 0.    |           1.    __ __ __      2.       |       3.    __ __ __|
 *       |__              |                       |
 *
 *
 *
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 * */
public enum Rotation {

    droite("Rota 90°",1),

    bas("Rota 180°",2),

    gauche("Rota -90°",3),

    haut("Rota 0°",0);

    //l'angle en degré de la rotation
    private String description;

    //le numéro de séquence de la rotation
    private int sequence;

    /**
     * Constructeur de l'énumération
     * @param descript La description de la rotation sous le format d'une chaine de caractère
     * @param seq Le numéro de séquence de la rotation
     **/
    private Rotation ( String descript , int seq ) {
        this.description = descript;
        this.sequence = seq;
    }

    //Getteurs et setteurs afin de modifier et récupérer la description et le numéro de séquence de la rotation.
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}
    public int getSequence() {return sequence;}
    public void setSequence(int sequence) {this.sequence = sequence;}


}
