import java.awt.*;

/**
 * Cette classe représente les données d'une figure, sa couleur, son typer et sa rotation.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class FigureMeta {

    //La couleur de la figure
    private Color color;

    //Le style de figure (la famille de la figure, carre, L, L inversé...)
    private Style style;

    //La rotation actuelle (dynamique) de la figure
    private Rotation rota;





    //Constructeur selon différentes façon

    /**
     * Constructeur de la classe avec une couleur et un style et une rotation spécifié
     * @param color La couleur de la figure
     * @param style Le style de la nouvelle figure
     * @param rota La rotation actuelle de la classe
     */
    public FigureMeta ( Color color , Style style, Rotation rota ) {
        if ( color != null ) {
            this.color = color;
        }
        if ( style != null ) {
            this.style = style;
        }
        this.rota = rota;
    }


    /**
     * Constructeur de la classe sans que la rotation ne sois spécifié, ainsi la rotaion est de 0° par défaut
     * @param color La couleur de la figure
     * @param style Le style de la nouvelle figure
     */
    public FigureMeta ( Color color , Style style ) {
        if ( color != null ) {
            this.color = color;
        }
        if ( style != null ) {
            this.style = style;
        }
        this.rota = Rotation.haut;
    }








    // . | . | _
    /**
     * Methode pour générer des données de figure aléatoirement en spécifiant la couleur et le style de cette figure, laisant la rotaion aléatoire
     * @param color La couleur de la figure
     * @param style Le style de la nouvelle figure
     * @return Les données de figure générés à moitié aléatoirement
     */
    public static FigureMeta FigureMetaAuto ( Color color , Style style ) {return new FigureMeta(color, style, Alea.RotaAuto());}

    // . | _ | _
    /**
     * Methode pour générer des données de figure en spécifiant la couleur mais en laissant le style et la rotation aléatoire
     * @param color La couleur de la figure
     * @return Les données de figure générés à moitié aléatoirement
     */
    public static FigureMeta FigureMetaAuto ( Color color ) {return new FigureMeta(color, Alea.StyleAuto(), Alea.RotaAuto());}

    // _ | . | _
    /**
     * Methode pour générer des données de figure aléatoirement en spécifiant le style de cette figure, laisant la rotaion et la couleur aléatoire
     * @param style Le style de la nouvelle figure
     * @return Les données de figure générés à moitié aléatoirement
     */
    public static FigureMeta FigureMetaAuto ( Style style ) {return new FigureMeta(Alea.ColorAuto(), style, Alea.RotaAuto());}

    // _ | . | .
    /**
     * Methode pour générer des données de figure aléatoirement en spécifiant la rotation et le style de cette figure, laisant la couleur aléatoire
     * @param style Le style de la nouvelle figure
     * @param rota La rotation de la nouvelle figure
     * @return Les données de figure générés à moitié aléatoirement
     */
    public static FigureMeta FigureMetaAuto ( Style style, Rotation rota ) {return new FigureMeta(Alea.ColorAuto(), style, rota);}

    // _ | _ | _
    /**
     * Methode pour créer de façon totalement aléatoirement une figure, la couleur le style et la rotation sont choisi aléatoirement
     * @return Les données de figure générés totalement aléatoirement
     */
    public static FigureMeta FigureMetaAuto () {return new FigureMeta(Alea.ColorAuto(), Alea.StyleAuto(), Alea.RotaAuto());}






    //Getteurs et setteurs des attributs de la classe
    public Color getColor () {return this.color;}
    public Style getStyle() {return this.style;}
    public int getNbRota() {return this.style.getNbRota();}
    public Rotation getRota() {return this.rota;}
	  public void setColor(Color color) {this.color = color;}
    public void setRota(Rotation rota) {this.rota = rota;}

    /**
     * Méthode pour obtenir les informations stocké par l'objet sous la forme d'un String. Elle retourne les trois attributs suivi de leurs valeur.
     * @return les attribut style, couleur et rotation  suivi de leurs valeurs sous le format d'un String.
     **/
    public String toString() {return "Style:"+this.style+" Color:"+this.color+" Rota:"+this.rota;}

}
