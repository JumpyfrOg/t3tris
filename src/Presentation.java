import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.text.*;


/**
 * Classe qui va créer des composants pour les placé sur la fenêtre de l'application. Cette classe se place juste derrière la fenêtre de l'application. Elle se place entre la fen^tre et les classes qui vont réagir aux actions de l'utilisateur.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public class Presentation {

    //La fenêtre de l'application. La fenêtre qui est affiché à l'écran et dont l'utilisateur intéragit.
    private JFrame frame;

    //Le chemin qui sépare le lieu de l'exécution du jeu du dossier contenant les données du jeu
    public String dataSource;

    //La grille qui contient des buttons représentant les cases de la grille de jeu
    public JButton[][] grille;

    //Tableau qui contiendra les panels qui seront positionés dans les deux grandes cases placées à gauche et à droite de la grille principale, et les 3 panels affiché sous la case situé à droite affichant les prochaines figures à suivre dans le jeu
    public JButton[][][] panneau;

    //Tableau de boutton qui contient les cinq boutons (servant de label)  afin d'afficher les minutes, les secondes et les deuc point séparateur du temps de la partie.
    private JButton[] temps;

    //La partie, le coté algorithmique du jeu
    public Game partie;

    //La classe qui s'occupe des intéractions humaines sur le jeu
    public ListenerKey list;

    //L'objet qui génère des évenements à chaque secondes
    private javax.swing.Timer timerPerso;


    /**
     * Le constructeur de la classe qui va initialiser ses attributs et vas remplir la fenêtre de l'application de composants pour que l'utilisateur puisse réagir.
     */
    public Presentation ( JFrame frame, String dataSource ) {
        this.frame = frame;
        this.dataSource = dataSource;

        this.initComponents();
    }

    /**
     * Méthode qui va initialiser la fenêtre de jeu pour faire apparaitre l'ath du jeu.
     */
    public void initComponents() {
        this.initATH();
        this.partie = new Game();
        this.partie.enCours().include(this);
        this.initButton();
        this.list = new ListenerKey(this);




        /**
         * Classe interne qui prend en charge les évenements généré par le timer qui sert de chronomètre pour la durée de la partie
         **/
        class TimerPerso implements ActionListener {
            //le gestionnaire de la fenêtre de jeu
            Presentation pre;
            //le timer qui génère un ActionEvent chaque seconde
            javax.swing.Timer tim;

            /**
             * Constructeur qui demande le gestionaire de la fenêtre de jeu pour lui demander des modifications sur la fenêtre ainsi que le timer qui génére des ActionEvent toute les secondes.
             * @param pre Le gestionnaire de la fenêtre de jeu
             * @param tim Le timer qui génère des ActionEvent toutes les secondes
             **/
            public TimerPerso (Presentation pre, javax.swing.Timer tim) {
                super();
                this.pre = pre;
                this.tim = tim;
            }

            /**
             * Méthode qui va être exécuté lors de la génération d'un actionEvent
             **/
            public void actionPerformed(ActionEvent e) {
                pre.secondePasse();
            }
        }

        //création du timer
        javax.swing.Timer timer = new javax.swing.Timer(1000,null);
        //création du timerPerso qui surveille le timer, et réagit en communiquant avec cette classe (Presentation)
        TimerPerso timerPerso = new TimerPerso(this,timer);
        //ajout de l'action de surveiller le timer
        timer.addActionListener(timerPerso);
        //démarage du timer
        timer.start();
        this.timerPerso = timer;

    }


    /**
     * Méthode va initialiser l'ATH du jeu c'est à dire tout les composants physiques qui ne changeront pas durant le jeu,
     * il sont juste là pour caractériser et donner forme au visuel de l'application
     */
    public void initATH () {

        JLayeredPane lp = new JLayeredPane();
        lp.setBounds(0,0,(int)this.frame.getSize().getWidth(),(int)this.frame.getSize().getHeight());
        lp.setOpaque(false);

        //taille des blocks 33 width, 30 height

        //82% de la taille de l'image originale -> 358/700
        JLabel label = new JLabel(new ImageIcon(new ImageIcon(this.dataSource+"images/background.jpg").getImage().getScaledInstance((int)this.frame.getSize().getWidth(),(int)this.frame.getSize().getHeight()-20, Image.SCALE_DEFAULT)));
        //200,5,358,700
        label.setBounds(-5, -10, (int)this.frame.getSize().getWidth(),(int)this.frame.getSize().getHeight());
        this.frame.add(label);

        //la grille de bouton représentant la grille de jeu où défile les figures.
        this.grille = new JButton[20][10];
        JPanel pan = new JPanel();
        pan.setOpaque(false);
        pan.setLayout(new GridLayout(20,10,1,0));
        for ( int i = 0 ; i < this.grille.length ; i++ ) {
            for (int j = 0 ; j < this.grille[i].length ; j++ ) {
                JButton tmp = new JButton();
                //tmp.setBorderPainted(false);
                tmp.setContentAreaFilled(false);
                tmp.setOpaque(false);
                tmp.setFocusPainted(false);
                tmp.setBorder(BorderFactory.createLineBorder(new Color(43,47,58),1));
                tmp.setBorderPainted(false);
                this.grille[i][j] = tmp;
                pan.add(tmp);
            }
        }
        pan.setBounds(213,26,330,680);
        pan.revalidate();
        pan.repaint();

        lp.add(label, new Integer(0));
        lp.add(pan, new Integer(1));


        //les grilles annexe, la grille de bouton pour afficher la figure sauvé par le joueur, et quatres autres grilles de bouton pour affiché les prochaines figures à jouer
        this.panneau = new JButton[5][4][4];
        int size = 4;


        //les panneau qui seront dans les encadrés latéraux
        JPanel gauche = new JPanel();
        JPanel droite = new JPanel();
        gauche.setBounds(57,55,80,80);
        droite.setBounds(620,55,80,80);
        gauche.setOpaque(false);
        droite.setOpaque(false);
        gauche.setLayout(new GridLayout(size,size));
        droite.setLayout(new GridLayout(size,size));


        lp.add(gauche, new Integer(1));
        lp.add(droite, new Integer(1));


        //les cases qui montrent les prochaines figures à jouer
        JPanel droite1 = new JPanel();
        JPanel droite2 = new JPanel();
        JPanel droite3 = new JPanel();
        droite1.setBounds(620,180,80,80);
        droite2.setBounds(620,280,80,80);
        droite3.setBounds(620,380,80,80);
        droite1.setOpaque(false);
        droite2.setOpaque(false);
        droite3.setOpaque(false);
        droite1.setLayout(new GridLayout(size,size));
        droite2.setLayout(new GridLayout(size,size));
        droite3.setLayout(new GridLayout(size,size));
        for ( int j = 0 ; j < 5 ; j++ ) {
            for ( int i = 0 ; i < size ; i++ ) {
                for ( int k = 0 ; k < size ; k++ ) {
                    JButton tmp = new JButton();
                    //tmp.setBorderPainted(false);
                    tmp.setContentAreaFilled(false);
                    tmp.setOpaque(false);
                    tmp.setFocusPainted(false);
                    tmp.setBorder(BorderFactory.createLineBorder(new Color(43,47,58),1));
                    tmp.setBorderPainted(false);
                    this.panneau[j][i][k] = tmp;

                    if ( j == 0 ) {
                        gauche.add(tmp);
                    }else if ( j == 1 ) {
                        droite.add(tmp);
                    }else if ( j == 2 ) {
                        droite1.add(tmp);
                    }else if ( j == 3 ) {
                        droite2.add(tmp);
                    }else if ( j == 4 ) {
                        droite3.add(tmp);
                    }
                }
            }
        }



        //initialisation des boutons pour afficher le temps de la partie
        this.temps = new JButton[5];
        for ( int i = 0 ; i < this.temps.length ; i++ ) {
          this.temps[i] = new JButton();
        }
        //placement des boutons dans la fenêtre
        this.temps[0].setBounds(0,0,40,80);
        this.temps[1].setBounds(40,0,40,80);
        this.temps[2].setBounds(80,0,20,80);
        this.temps[3].setBounds(100,0,40,80);
        this.temps[4].setBounds(140,0,40,80);

        //creation du panel qui contiendra les boutons
        JPanel time = new JPanel();
        time.setLayout(null);
        time.setBounds(565,640,180,80);
        time.setOpaque(false);

        //affection des immages aux boutons et configuration pour les rendres invisible et n'afficher que les images qu'ils contiennent.
        for ( int i = 0 ; i < this.temps.length ; i++ ) {
          time.add(this.temps[i]);
          this.temps[i].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/0.png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
          this.temps[i].setContentAreaFilled(false);
          this.temps[i].setOpaque(false);
          this.temps[i].setFocusPainted(false);
          this.temps[i].setBorderPainted(false);
        }
        this.temps[2].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/dot.png").getImage().getScaledInstance(20,80, Image.SCALE_DEFAULT)));


        //Ajout des différents panel à la fenêtre de jeu
        lp.add(time, new Integer(1));
        lp.add(droite1, new Integer(1));
        lp.add(droite2, new Integer(1));
        lp.add(droite3, new Integer(1));


        this.frame.add(lp);




        this.frame.validate();
        this.frame.repaint();
        this.frame.setVisible(true);
    }


    /**
     * Méthode pour afficher dans les grilles de bouton, les blocks de la figure en cours, ainsi que des prochaines figure à jouer.
     */
    public void initButton() {
        Block [][] board = this.partie.getBoard();
        String color = this.colorToString(this.partie.enCours().getData().getColor());
        int[][] blocks = this.partie.enCours().getOccupedCube();

        //afficher les blocks de la figures en cours
        for ( int i = 0 ; i < blocks.length ; i++ ) {
            this.grille[blocks[i][0]][blocks[i][1]].setIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png"));
            this.grille[blocks[i][0]][blocks[i][1]].setContentAreaFilled(true);
        }

        //afficher les blocks de la figure sauvé, puis des prochaines figures
        for ( int i = 0 ; i < this.partie.getNext()[0].getOccupedCube().length ; i++ ) {
            color = this.colorToString(this.partie.getNext()[0].getData().getColor());
            this.panneau[1][this.partie.getNext()[0].getOccupedCube()[i][0]][this.partie.getNext()[0].getOccupedCube()[i][1]-3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png").getImage().getScaledInstance(20,20, Image.SCALE_DEFAULT)));
            this.panneau[1][this.partie.getNext()[0].getOccupedCube()[i][0]][this.partie.getNext()[0].getOccupedCube()[i][1]-3].setContentAreaFilled(true);
        }
        for ( int i = 0 ; i < this.partie.getNext()[1].getOccupedCube().length ; i++ ) {
            color = this.colorToString(this.partie.getNext()[1].getData().getColor());
            this.panneau[2][this.partie.getNext()[1].getOccupedCube()[i][0]][this.partie.getNext()[1].getOccupedCube()[i][1]-3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png").getImage().getScaledInstance(20,20, Image.SCALE_DEFAULT)));
            this.panneau[2][this.partie.getNext()[1].getOccupedCube()[i][0]][this.partie.getNext()[1].getOccupedCube()[i][1]-3].setContentAreaFilled(true);
        }
        for ( int i = 0 ; i < this.partie.getNext()[2].getOccupedCube().length ; i++ ) {
            color = this.colorToString(this.partie.getNext()[2].getData().getColor());
            this.panneau[3][this.partie.getNext()[2].getOccupedCube()[i][0]][this.partie.getNext()[2].getOccupedCube()[i][1]-3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png").getImage().getScaledInstance(20,20, Image.SCALE_DEFAULT)));
            this.panneau[3][this.partie.getNext()[2].getOccupedCube()[i][0]][this.partie.getNext()[2].getOccupedCube()[i][1]-3].setContentAreaFilled(true);
        }
        for ( int i = 0 ; i < this.partie.getNext()[0].getOccupedCube().length ; i++ ) {
            color = this.colorToString(this.partie.getNext()[3].getData().getColor());
            this.panneau[4][this.partie.getNext()[3].getOccupedCube()[i][0]][this.partie.getNext()[3].getOccupedCube()[i][1]-3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png").getImage().getScaledInstance(20,20, Image.SCALE_DEFAULT)));
            this.panneau[4][this.partie.getNext()[3].getOccupedCube()[i][0]][this.partie.getNext()[3].getOccupedCube()[i][1]-3].setContentAreaFilled(true);
        }

    }


    /**
     * Méthode pour mettre à jour l'affichage de la figure sauvegardé
     */
    public void updateSaveBlock () {
        //récupération des coordonnées intiales de la figure, ainsi que sa couleur
        int[][] coor = StaticValue.getCoordonatInit(this.partie.getSave().getData());
        String color = this.colorToString(this.partie.getSave().getData().getColor());

        //Remise à zéro des boutons afin de les mettre dans leur état d'initialisation
        for ( int i = 0 ; i < this.panneau[0].length ; i++ ) {
            for ( int j = 0 ; j < this.panneau[0][i].length ; j++ ) {
                this.panneau[0][i][j].setIcon(null);
                this.panneau[0][i][j].setContentAreaFilled(false);
            }
        }

        //affichage des blocks avec leur couleurs en décalant les coordonées de placement qui sont décalé entre une grille annexe et la grile principale bien plus large
        for ( int i = 0 ; i < coor.length ; i++ ) {
            this.panneau[0][coor[i][0]][coor[i][1]-3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png").getImage().getScaledInstance(20,20, Image.SCALE_DEFAULT)));
            this.panneau[0][coor[i][0]][coor[i][1]-3].setContentAreaFilled(true);

            //System.out.println(java.util.Arrays.toString(coor[i]));
        }
    }


    /**
     * Attribut l'image en couleur de la case dont les coordonnées sont passés en paramètres, selon ce que contient cette case dans le tableau de la partie
     * @param x La ligne de la case
     * @param y La colonne de la case
     */
    public void setColor (int x, int y) {
        //case vide
        if ( this.partie.getBoard()[x][y] == null ) {
            this.grille[x][y].setIcon(null);
            this.grille[x][y].setContentAreaFilled(false);

        //case occupé par une figure
        }else{
            String color = this.colorToString(this.partie.getBoard()[x][y].getFigure().getData().getColor());
            this.grille[x][y].setIcon(new ImageIcon(this.dataSource+"images/block/block"+color+".png"));
            this.grille[x][y].setContentAreaFilled(true);
        }
    }


    /**
     * Méthode exécuté lorsque la prochaine figure à jouer doit être placé sur le plateau. La file de figure avance donc avec l'ajout d'une nouvelle figure.
     */
    public void playNext () {
        //vérification que le joueur n'a pas remplis une ligne afin de la supprimer
        this.partie.checkLine();

        //Récupération de score et convertion au format String avec un espace entre chhaque milier
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.FRENCH);
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        formatter.setDecimalFormatSymbols(symbols);
        String scoreEspace = formatter.format(this.partie.getScore());
        //mis à jour de l'affichage du score dans le titre de la fenêtre
        this.frame.setTitle("T3tris - "+scoreEspace+" pts");

        //vérification de la posibilité de placer la prochaine figure sur le plateau selon les règles (que la figure ne chevauche pas une autre lors de son placement initial)
        int [][] cube = this.partie.getNext()[0].getOccupedCube();
        boolean canPlace = true;
        for ( int i = 0 ; i < cube.length ; i++ ) {
            if ( this.partie.getBoard()[cube[i][0]][cube[i][1]] != null ) {
                canPlace = false;
            }
        }

        //s'il est impossible de placer la prochaine figure, le joueur a perdu la partie
        if ( !canPlace ) {
          //arrêt des timers
          this.list.chrono.stop();
          this.timerPerso.stop();
          JOptionPane.showMessageDialog(this.frame,"Perdu");


        //sinon on place la prochaine figure
        }else{
            //décalage des prochaines figures à jouer
            this.partie.next();
            //ajouter la prochaine figure à jouer, devenu la figure en cours, sur le plateau de jeu
            this.partie.enCours().include(this);

            //mettre les buttons d'affichage des 4 prochaines figures à l'état d'initialisé
            for ( int j = 1 ; j < this.panneau.length ; j++ ) {
                for ( int i = 0 ; i < this.panneau[j].length ; i++ ) {
                    for ( int k = 0 ; k < this.panneau[j][i].length ; k++ ) {
                        this.panneau[j][i][k].setContentAreaFilled(false);
                        this.panneau[j][i][k].setIcon(null);
                    }
                }
            }

            //afficher les nouvelles figures prochainement jouable dans les cases à droite de la fenêtre
            this.initButton();
            this.reframeBoard();
        }
    }



    /**
     * Méthode pour mettre a jour toutes les cases du tableau pour afficheer les cases occupées
     */
    public void reframeBoard () {
        for ( int i = 0 ; i < this.partie.getBoard().length ; i++ ) {
            for ( int j = 0 ; j < this.partie.getBoard()[i].length ; j++ ) {
                this.setColor(i, j);
            }
        }
    }


    /**
     * Méthode pour mettre à jour la fenetre du jeu afin d'appliquer les changements visuel effectué
     **/
    public void reframe () {
        this.frame.revalidate();
        this.frame.repaint();
    }


    /**
     * Méthode qui retourne une chaine de caractère représentant la couleur passé en paramètre. La couleur passé en paramètre est un objet "Color" du package java.util.
     * @param color L'objet couleur normalement attribuer à une figure
     * @return La couleur passé paramètre sous la forme d'une chaine de caractère
     **/
    public String colorToString ( Color color ) {
        String ret = null;
        if ( color == Color.blue ) {
            ret = "Blue";
        }else if ( color == Color.green ) {
            ret = "Green";
        }else if ( color == Color.cyan ) {
            ret = "Cyan";
        }else if ( color == Color.orange ) {
            ret = "Orange";
        }else if ( color == Color.magenta ) {
            ret = "Magenta";
        }else if ( color == Color.red ) {
            ret = "Red";
        }else if ( color == Color.yellow ) {
            ret = "Yellow";
        }
        return ret;
    }


    /**
     * Métode qui va augmenter le nombre de seconde du temps écouler et éventuellement les minutes selon le nombre de seconde déjà écoulé, puis change les images affichés dans les boutons dédiés pour mettre à jour l'affichage du temps écoulé.
     **/
    public void secondePasse () {
        //augmentation du temps écoulé (sauvé dans la classe Game) de 1 seconde
        this.partie.secondePlus();

        //récupération du temps sauvé
        int minute = this.partie.getMinute();
        int seconde = this.partie.getSeconde();

        //gestion de l'affichage des minutes
        //si les minutes ne dépasse pas l'affichage de deux chiffres
        if ( minute < 10 ) {
            this.temps[1].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/"+minute+".png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
            //sinon les minutes sont composées de deux chiffres
        }else{
            //conversion du nombre de minute en String
            String minutes = minute+"";
            //récupération des deux chiffres qui compossent les minutes sauvées
            int dizaine = Integer.parseInt(minutes.substring(0,1));
            int unite = Integer.parseInt(minutes.substring(1,2));
            this.temps[0].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/"+dizaine+".png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
            this.temps[1].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/"+unite+".png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
        }

        //gestion de l'affichage des secondes
        //si les secondes ne dépasse pas l'affichage de deux chiffres
        if ( seconde < 10 ) {
            this.temps[3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/0.png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
            this.temps[4].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/"+seconde+".png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
            //sinon les secondes sont composées de deux chiffres
        }else{
            //conversion du nombre de seconde en String
            String secondes = seconde+"";
            //récupération des deux chiffres qui compossent les secondes sauvées
            int dizaine = Integer.parseInt(secondes.substring(0,1));
            int unite = Integer.parseInt(secondes.substring(1,2));
            this.temps[3].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/"+dizaine+".png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
            this.temps[4].setIcon(new ImageIcon(new ImageIcon(this.dataSource+"images/numero/"+unite+".png").getImage().getScaledInstance(40,80, Image.SCALE_DEFAULT)));
        }
    }

    //Getteur pour récupérer la fenêtre de l'application d'où cette classe apllique des modifications
    public JFrame getFenetre(){return this.frame;}
}
