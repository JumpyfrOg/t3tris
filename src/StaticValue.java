/**
 * Classe abstraite qui contient des méthodes retournant des informations nécessaire pour le fonctionnement du jeu.
 *
 * @author Charron M.
 * @version 2.0
 * License : GPL or later
 **/
public abstract class StaticValue {

    /**
     * Méthode pour récupérer les coordonées des blocks d'une figure dont le style est passé par paramètre. Ces coordonnées sont les coordonnées initialises
     * afin d'initialiser une figure.
     * @param meta Les méta-données d'une figure, dont le style y est stocké
     * @return Le tableau de coordonées initialisé d'une figure
     */
    public static int [][] getCoordonatInit ( FigureMeta meta ) {
        int[][] ret = null;
        if ( meta != null ) {

            //si la métadonnées est une barre
            if ( meta.getStyle().equals(Style.barre) ) {
                ret = new int [4][2];
                if ( meta.getRota().equals(Rotation.haut) || meta.getRota().equals(Rotation.bas) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 5;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 2;
                    ret [2][1] = 5;

                    ret [3][0] = 3;
                    ret [3][1] = 5;
                }else if ( meta.getRota().equals(Rotation.gauche) || meta.getRota().equals(Rotation.droite) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 3;

                    ret [1][0] = 0;
                    ret [1][1] = 4;

                    ret [2][0] = 0;
                    ret [2][1] = 5;

                    ret [3][0] = 0;
                    ret [3][1] = 6;
                }
            }

            //si la métadonnées est un l avec le bas sur la droite
            if ( meta.getStyle().equals(Style.l_bas_droit) ) {
                ret = new int [4][2];
                if ( meta.getRota().equals(Rotation.haut) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 5;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 2;
                    ret [2][1] = 5;

                    ret [3][0] = 2;
                    ret [3][1] = 6;
                }else if ( meta.getRota().equals(Rotation.droite) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 6;

                    ret [1][0] = 0;
                    ret [1][1] = 5;

                    ret [2][0] = 0;
                    ret [2][1] = 4;

                    ret [3][0] = 1;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.bas) ) {
                    ret [0][0] = 2;
                    ret [0][1] = 5;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 0;
                    ret [2][1] = 5;

                    ret [3][0] = 0;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.gauche) ) {
                    ret [0][0] = 1;
                    ret [0][1] = 4;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 1;
                    ret [2][1] = 6;

                    ret [3][0] = 0;
                    ret [3][1] = 6;
                }
            }

            //si la métadonnées est un l avec le bas sur la gauche
            if ( meta.getStyle().equals(Style.l_bas_gauche) ) {
                ret = new int [4][2];
                if ( meta.getRota().equals(Rotation.haut) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 5;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 2;
                    ret [2][1] = 5;

                    ret [3][0] = 2;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.droite) ) {
                    ret [0][0] = 1;
                    ret [0][1] = 6;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 1;
                    ret [2][1] = 4;

                    ret [3][0] = 0;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.bas) ) {

                    ret [0][0] = 2;
                    ret [0][1] = 4;

                    ret [1][0] = 1;
                    ret [1][1] = 4;

                    ret [2][0] = 0;
                    ret [2][1] = 4;

                    ret [3][0] = 0;
                    ret [3][1] = 5;
                }else if ( meta.getRota().equals(Rotation.gauche) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 4;

                    ret [1][0] = 0;
                    ret [1][1] = 5;

                    ret [2][0] = 0;
                    ret [2][1] = 6;

                    ret [3][0] = 1;
                    ret [3][1] = 6;
                }
            }

            //si la métadonnées est un carre
            if ( meta.getStyle().equals(Style.carre) ) {
                ret = new int [4][2];
                ret [0][0] = 0;
                ret [0][1] = 4;

                ret [1][0] = 0;
                ret [1][1] = 5;

                ret [2][0] = 1;
                ret [2][1] = 4;

                ret [3][0] = 1;
                ret [3][1] = 5;
            }

            //si la métadonnées est un carre
            if ( meta.getStyle().equals(Style.triangle) ) {
                ret = new int [4][2];
                if ( meta.getRota().equals(Rotation.haut) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 5;

                    ret [1][0] = 1;
                    ret [1][1] = 4;

                    ret [2][0] = 1;
                    ret [2][1] = 5;

                    ret [3][0] = 1;
                    ret [3][1] = 6;
                }else if ( meta.getRota().equals(Rotation.droite) ) {
                    ret [0][0] = 1;
                    ret [0][1] = 5;

                    ret [1][0] = 0;
                    ret [1][1] = 4;

                    ret [2][0] = 1;
                    ret [2][1] = 4;

                    ret [3][0] = 2;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.bas) ) {
                    ret [0][0] = 1;
                    ret [0][1] = 5;

                    ret [1][0] = 0;
                    ret [1][1] = 6;

                    ret [2][0] = 0;
                    ret [2][1] = 5;

                    ret [3][0] = 0;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.gauche) ) {
                    ret [0][0] = 1;
                    ret [0][1] = 4;

                    ret [1][0] = 2;
                    ret [1][1] = 5;

                    ret [2][0] = 1;
                    ret [2][1] = 5;

                    ret [3][0] = 0;
                    ret [3][1] = 5;
                }
            }

            //si la métadonnées est un z ou le bas est à droite
            if ( meta.getStyle().equals(Style.z_bas_droit) ) {
                ret = new int [4][2];
                if ( meta.getRota().equals(Rotation.haut) || meta.getRota().equals(Rotation.bas) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 4;

                    ret [1][0] = 1;
                    ret [1][1] = 4;

                    ret [2][0] = 1;
                    ret [2][1] = 5;

                    ret [3][0] = 2;
                    ret [3][1] = 5;
                }else if ( meta.getRota().equals(Rotation.droite) || meta.getRota().equals(Rotation.gauche) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 6;

                    ret [1][0] = 0;
                    ret [1][1] = 5;

                    ret [2][0] = 1;
                    ret [2][1] = 5;

                    ret [3][0] = 1;
                    ret [3][1] = 4;
                }
            }

            //si la métadonnées est un z ou le bas est à gauche
            if ( meta.getStyle().equals(Style.z_bas_gauche) ) {
                ret = new int [4][2];
                if ( meta.getRota().equals(Rotation.haut) || meta.getRota().equals(Rotation.bas) ) {
                    ret [0][0] = 0;
                    ret [0][1] = 5;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 1;
                    ret [2][1] = 4;

                    ret [3][0] = 2;
                    ret [3][1] = 4;
                }else if ( meta.getRota().equals(Rotation.droite) || meta.getRota().equals(Rotation.gauche) ) {
                    ret [0][0] = 1;
                    ret [0][1] = 6;

                    ret [1][0] = 1;
                    ret [1][1] = 5;

                    ret [2][0] = 0;
                    ret [2][1] = 5;

                    ret [3][0] = 0;
                    ret [3][1] = 4;
                }
            }
        }
        return ret;
    }



    /**
     * Méthode pour récuperer le numéro du block qui sert d'ancre pour la figure (pour la rotation) selon la figure passé en paramètre
     * @param style Le style d'une figure dont on cherche l'ancre.
     * @return Le numéro du block qui sert d'ancre.
     */
    public static int getNumBlockAnchor (Style style) {
        int ret = -1;
        //Barre
        if ( style == Style.barre ) {
            ret = 2;
        }else

        //Carre
        if ( style == Style.carre ) {
            ret = 3;
        }

        //L bas droit
        if ( style == Style.l_bas_droit ) {
            ret = 1;
        }else

        //L bas gauche -> _|
        if ( style == Style.l_bas_gauche ) {
            ret = 1;
        }else

        //Triangle
        if ( style == Style.triangle ) {
            ret = 2;
        }else

        //z bas droit
        if ( style == Style.z_bas_droit )  {
            ret = 2;
        }else

        //z bas gauche
        if ( style == Style.z_bas_gauche )  {
            ret = 1;
        }

        return ret;
    }

}
